import { useEffect } from "react";
import { Card } from "./components/Card";
import { Cart } from "./components/Cart";
import { Container } from "./components/styles/Container.styled"
import { Flex } from "./components/styles/Flex.styled"
import { Loader } from "./components/styles/Loader.styled";
import { useAppDispatch, useAppSelector } from "./store/hooks"
import { getProducts } from "./store/slices/product/product-slice";
import { ProductList } from "./store/slices/product/ProductListView";

function App() {

  return (
    <>
      <Container>
        <Flex container={true}>
          <ProductList />
        </Flex>
      </Container>
      <Cart/>
    </>
  )
}

export default App
