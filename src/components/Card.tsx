import { useEffect, useState } from "react";
import { currency } from "../common/helper/helper";
import { Cart } from "../models/cart.interface";
import { Product } from "../models/product.interface"
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { addItem, getCart, openCart } from "../store/slices/cart/cart-slice";
import { Button } from "./styles/Button.styled";
import { StyledCard } from "./styles/Card.styled";

import { StyledCardImage } from "./styles/CardImage.styled";
import { StyledCardTitle } from "./styles/CardTitle.styled";


export const Card: React.FC<Product> = ({ title, id, type, rating, price, image, onSale, sell }) => {
    const [favorite, setFavorite] = useState(false);

    const cartState = useAppSelector((state) => state.cartState);
    const dispatch = useAppDispatch();

    const addToCart = () => {
        dispatch(openCart());
        let cartItem: Cart = {
            id: 0,
            product: { title, id, type, rating, price, image, onSale, sell },
            quantity: 1,
            total: onSale && !!sell ? price * sell / 100 : price,
            size: "Medium",
        }
        dispatch(addItem(cartItem));
        dispatch(getCart());
    }


    return (<StyledCard>
        <StyledCardTitle>
            <div>{onSale ? <p>SALE</p> : ''}</div>
            <div onClick={() => { setFavorite(!favorite) }}><span className="material-icons ">
                {favorite ? 'favorite' : 'favorite_border'}
            </span></div>
        </StyledCardTitle>
        <StyledCardImage src={image} />
        <p>{type}</p>
        <div>{title}</div>
        <div>
            {Array.from({ length: 5 }, (_, i) => i >= rating ? <span key={i} className="material-icons ">
                star_border
            </span> : <span key={i} className="material-icons ">
                star
            </span>)}

        </div>
        <Button onClick={addToCart}>
            <div>
                Add To Cart
            </div>
            <div>
                {"$" + currency(price)}
            </div>
        </Button>
    </StyledCard>)
}