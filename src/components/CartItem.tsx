import { useEffect, useState } from "react";
import { currency } from "../common/helper/helper";
import { Cart } from "../models/cart.interface";
import { useAppDispatch } from "../store/hooks";
import { updateCart, deleteItem } from "../store/slices/cart/cart-slice";
import { CartImage } from "./styles/CartImage.styled";
import { StyledCartItem } from "./styles/CartItem.styled";


export const CartItem: React.FC<Cart> = ({ id, total, product, size, quantity }) => {
    const [itemQuantity, setQuantity] = useState(quantity);
    const [itemPrice, setPrice] = useState(total);
    const [hide, setHide] = useState(true);

    const dispatch = useAppDispatch();

    enum Action { increment, decriment };

    const updateQuantity = (aciton: Action) => {
        if (aciton == Action.decriment)
            itemQuantity > 1 && setQuantity(itemQuantity - 1);
        else if (aciton == Action.increment)
            setQuantity(itemQuantity + 1);
    }

    const deleteCartItem = () => {
        setHide(false);
        dispatch(deleteItem(id));
    }

    useEffect(() => {
        let currentPrice: number = 0;
        if (product.onSale && !!product.sell) {
            currentPrice = product.price * product.sell * itemQuantity / 100;
        } else {
            currentPrice = product.price * itemQuantity;
        }
        setPrice(currentPrice);

        dispatch(updateCart({ id, cartItem: { id, total: currentPrice, product, size, quantity: itemQuantity } }));

    }, [itemQuantity]);

    return <StyledCartItem open={hide}>
        <CartImage src={product.image} />
        <div>
            <div>{product.title}</div>
            <span>Size: <span>{size}</span> </span>
            <div className="currency">$
                {
                    currency(itemPrice)
                }
                <span>
                    {
                        product.onSale ? "$" + currency(product.price * itemQuantity) : ""
                    }
                </span>
            </div>
        </div>
        <section>

            <span className="material-icons" onClick={deleteCartItem}>
                delete_outline
            </span>

            <div>
                <span className="material-icons" onClick={() => { updateQuantity(Action.decriment) }}>
                    remove
                </span>

                {itemQuantity}

                <span className="material-icons" onClick={() => { updateQuantity(Action.increment) }}>
                    add
                </span>
            </div>
        </section>
    </StyledCartItem>;
}