import { DefaultTheme, useTheme } from "styled-components";
import { currency } from "../common/helper/helper";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { closeCart } from "../store/slices/cart/cart-slice";
import { CartList } from "../store/slices/cart/CartListView";
import { Button } from "./styles/Button.styled";
import { StyledCart } from "./styles/Cart.styled";


export const Cart: React.FC = () => {
    const theme: DefaultTheme = useTheme();

    const cartState = useAppSelector((state) => state.cartState);
    const dispatch = useAppDispatch();

    return <StyledCart open={cartState.open}>
        <div>
            <header>
                <span className="material-icons" onClick={() => { dispatch(closeCart()); }}>
                    close
                </span>
                <div>
                    My cart
                    <img src="./images/cart.png" />
                </div>
            </header>
            <div>
                <CartList />
            </div>
            <footer>
                <p>Hey Get Free shipping on order over $250</p>
                <div>
                    <div>Sub total: <p>
                        ${cartState.loading == 'succeeded' ? currency(cartState.carts.reduce((a, d) => a + d.total, 0)) : "0"}
                    </p>
                    </div>
                    <Button bgColor={theme?.colors?.secondary} color={theme?.colors?.primary}>
                        <div>
                            Checkout
                        </div>
                    </Button>
                </div>
            </footer>
        </div >
    </StyledCart >;
}