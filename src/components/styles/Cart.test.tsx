import { describe, it } from "vitest";
import { renderTheme } from "../../styles/render-theme";
import "jest-styled-components";
import { StyledCart } from "./Cart.styled";

describe("Cart", () => {
  it("render currectly", () => {
    const cart = renderTheme(<StyledCart open={false}/>);
    expect(cart).toMatchSnapshot();
  });
});
