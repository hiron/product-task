import { describe, it } from "vitest";
import { renderTheme } from "../../styles/render-theme";
import { Button } from "./Button.styled";
import "jest-styled-components";

describe("Button", () => {
  it("render currectly", () => {
    const button = renderTheme(<Button color="red" bgColor="green"/>);
    expect(button).toMatchSnapshot();
  });
});
