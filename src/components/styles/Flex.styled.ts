import styled, { DefaultTheme } from "styled-components";
import { Container } from "./Container.styled";

type Props = {
  container?: boolean;
  xs?: number;
  sm?: number;
  md?: number;
  theme: DefaultTheme;
};


export const Flex = styled.div<Props>(({ container, md, xs, sm, theme }) => {
  return container
    ? `
    display: flex;
    box-sizing: border-box;
    flex-wrap: wrap;
    width: 100%;
    flex-direction: row;
    align-items: center;
  `
    : `
    padding: 14px;
    box-sizing: border-box;
    flex-basis:${((md || sm || xs || theme.grid) / theme.grid) * 100 + "%"};
    width:${((md || sm || xs || theme.grid) / theme.grid) * 100 + "%"};

    @media(max-width:${theme.layout.xs}){
      flex-basis:
        ${((xs || theme.grid) / theme.grid) * 100 + "%"};
      width:
        ${((xs || theme.grid) / theme.grid) * 100 + "%"};
    }

    @media(max-width:${theme.layout.sm}) and (min-width:${theme.layout.xs}){
      flex-basis:${((sm || xs || theme.grid) / theme.grid) * 100 + "%"};
      
      width:${((sm || xs || theme.grid) / theme.grid) * 100 + "%"};
    }

    @media(min-width: ${theme.layout.sm}){
      flex-basis:${((md || sm || xs || theme.grid) / theme.grid) * 100 + "%"};
      width:${((md || sm || xs || theme.grid) / theme.grid) * 100 + "%"};
    }

  `;
});
