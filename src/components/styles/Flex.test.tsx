import { describe, it } from "vitest";
import { renderTheme } from "../../styles/render-theme";
import { Button } from "./Button.styled";
import "jest-styled-components";
import { Flex } from "./Flex.styled";

describe("Flex", () => {
    it('Render Currectly the flex container', () => {
        const flex = renderTheme(<Flex container={true}></Flex>)
        expect(flex).toMatchSnapshot();
    });

    it('Render Currectly the flex Body', () => {
        const flex = renderTheme(<Flex xs={12} md={2} sm={6}></Flex>)
        expect(flex).toMatchSnapshot();
    });
})