import styled from "styled-components";

export const StyledCardTitle = styled.div`
  display: flex;
  box-sizing: border-box;
  flex-wrap: wrap;
  width: 100%;
  flex-direction: row;
  align-items: center;
  align-content: space-around;
  justify-content: space-between;
  margin-bottom: 45px;

  & > div > span {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.buttonBorder};
    &:hover{
      transform: scale(1.2);
      cursor: pointer;
    }

    &:active{
      transform: scale(0.98);
    }
  }

  & > div > p {
    color: ${({ theme }) => theme.colors.alert};
    font-size: 12px;
    font-weight: bold;
    padding: 4px;
    background-color: #DE36181A;
    border-radius: 4px;
    border: 0px solid #eee;
  }

`;
