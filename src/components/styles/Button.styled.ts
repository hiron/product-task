import styled from "styled-components";

type Props = {
  bgColor?: string;
  color?: string;
};

export const Button = styled.button<Props>`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 10px;

  border: 1px solid
    ${({ theme, bgColor }) =>
      !bgColor ? theme.colors.buttonBorder : "#00000000"};
  border-radius: 4px;
  background: ${({ theme, bgColor }) =>
    !bgColor ? theme.colors.primary : bgColor};
  color: ${({ color, theme }) => (!color ? theme.colors.font : color)};
  
  & > div {
    padding: 10px 15px;
    font-weight: 500;
    font-size: 16px;
    margin: 0 auto;
  }

  &:hover {
    background-color: ${({ theme }) => theme.colors.secondary};
    color: ${({ theme }) => theme.colors.primary};
    border: 0px;
    cursor: pointer;
  }

  &:active {
    opacity: 0.9;
    transform: scale(0.98);
  }
`;
