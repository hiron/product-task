import styled from "styled-components";

type Props = {
  src: string;
};
export const StyledCardImage = styled.div<Props>`
    height: 190px;
    width: 190px;
    box-sizing: border-box;
    margin: 0 auto;
    padding: 15px;
    background: url(./images/products/${({ src }) => src});
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;
`;
