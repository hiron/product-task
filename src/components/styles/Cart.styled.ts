import styled from "styled-components";

type Props = {
  open: boolean;
};

export const StyledCart = styled.div<Props>`
  background: rgba(0, 0, 0, 0.4);
  backdrop-filter: blur(8px);
  height: 100vh;
  width: ${({ open }) => (open ? "100%" : "0")};
  position: fixed;
  top: 0;
  right: 0;
  transition: opacity 0.5s ease-in-out;

  & > div {
    height: 100vh;
    width: ${({ open }) => (open ? "610px" : "0px")};
    position: fixed;
    top: 0;
    right: 0;
    z-index: 1;
    background-color: ${({ theme }) => theme.colors.primary};
    overflow-x: hidden;
    transition: 0.5s;

    & > header {
      display: flex;
      align-items: center;
      box-sizing: border-box;
      padding: 28px 30px;
      border-bottom: 1px solid ${({ theme }) => theme.colors.border};
      justify-content: space-between;

      & > div {
        flex-grow: 1;
        text-align: center;
        font-weight: 500;
        font-size: 22px;
        & > img {
          height: 22px;
          margin-left: 15px;
        }
      }

      & > span {
        cursor: pointer;

        &:hover {
          transform: scale(1.2);
        }
      }
    }

    & > div {
      padding: 0 30px 200px;
      box-sizing: border-box;
    }

    & > footer {
      box-sizing: border-box;
      padding: 30px;
      width: 610px;
      position: fixed;
      bottom: 0;
      background: ${({ theme }) => theme.colors.primary};

      & > p {
        box-sizing: border-box;
        background: ${({ theme }) => theme.colors.focus};
        padding: 13px;
        border-radius: 5px;
        text-align: center;
        margin: 2px;
      }

      & > div {
        box-sizing: border-box;
        margin: 2px;
        padding-top: 20px;
        display: flex;

        & > div {
          flex: 1;
          color: ${({ theme }) => theme.colors.secondaryFont};
          font-size: 18px;
          font-weight: 400;

          & > p {
            font-weight: 600;
            font-size: 20px;
            color: ${({ theme }) => theme.colors.font};
          }
        }

        & > button {
          flex: 1;
        }
      }
    }
  }

  @media (max-width: ${({ theme }) => theme.layout.xs}) {
    & > div {
      max-width: 70%;
      & > div > div {
        flex-direction: column;
        align-items: center;
      }
      & > footer {
        max-width: 70%;
      }
    }
  }
`;
