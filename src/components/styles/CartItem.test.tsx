import { describe, it } from "vitest";
import { renderTheme } from "../../styles/render-theme";
import "jest-styled-components";
import { StyledCartItem } from "./CartItem.styled";

describe("Cart Item", () => {
  it("render currectly", () => {
    const cartItem = renderTheme(<StyledCartItem open={true}/>);
    expect(cartItem).toMatchSnapshot();
  });
});
