import styled from "styled-components";
type Props = {
  src: string;
};
export const CartImage = styled.div<Props>`
  width: 112px;
  height: 112px;
  margin: 2px;
  background: ${({ theme }) => theme.colors.focus};
  border-radius: 5px;
  background-image: url(./images/products/${({ src }) => src});
  background-position: center;
  background-size: auto 90px;
  background-repeat: no-repeat;
`;
