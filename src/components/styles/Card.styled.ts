import styled from "styled-components";

export const StyledCard = styled.div`
  padding: 20px;
  border: 1px solid ${({ theme }) => theme.colors.border};
  box-sizing: border-box;
  border-radius: 8px;
  display: flex;
  align-items: flex-start;
  background-color: ${({ theme }) => theme.colors.primary};
  flex-direction: column;
  min-width: 190px;

  &:hover {
    box-shadow: 0px 20px 24px rgba(0, 0, 0, 0.04);
    transform: scale(1.02);
  }

  & > div {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.font};
  }

  & > p {
    margin-top: 40px;
    margin-bottom: 8px;
    font-size: 12px;
    color: ${({ theme }) => theme.colors.secondaryFont};
  }

  & > div > span {
    margin: 8px 0;
    font-size: 16px;
    padding-right: 4px;
    color: ${({ theme }) => theme.colors.yellow};
  }
`;
