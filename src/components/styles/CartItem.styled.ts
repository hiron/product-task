import styled from "styled-components";

type Props = {
  open: boolean;
};
export const StyledCartItem = styled.div<Props>`
  display: ${({ open }) => (open ? "flex" : "none")};
  box-sizing: border-boxing;
  padding: 20px 0;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};

  & > section {
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: end;
    gap: 40px;

    & > div {
      border: 1px solid ${({ theme }) => theme.colors.buttonBorder};
      border-radius: 5px;
    }

    & > div > span {
      padding-left: 10px;
      padding-right: 10px;
      vertical-align: middle;
    }

    & span {
      cursor: pointer;
      margin: 3px;
      &:hover {
        transform: scale(1.2);
      }
    }
  }

  & > div {
    margin-right: 20px;
    font-size: 18px;
    line-height: 35px;

    & > span {
      font-size: 14px;
      & > span {
        color: ${({ theme }) => theme.colors.secondaryFont};
      }
    }

    & > div.currency {
      font-weight: 600;
    }

    & > div > span {
      color: ${({ theme }) => theme.colors.secondaryFont};
      margin-left: 20px;
      text-decoration: line-through;
    }
  }

  @media (max-width: ${({ theme }) => theme.layout.xs}) {
    & > section {
      flex-direction: row;
    }
  }
`;
