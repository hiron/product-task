import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "./global.styles";
import { theme } from "./theme";

type Props = {
    children: React.ReactNode,
};

export const StyledThemeProvider: React.FC<Props> = ({ children }) => {
    return (
        <ThemeProvider theme={theme}>
            <GlobalStyles />
            {children}
        </ThemeProvider>
    );
};