import { render } from "@testing-library/react";
import React from "react";
import { Button } from "../components/styles/Button.styled";
import { StyledThemeProvider } from "./styled-theme-provider";

export const renderTheme = (ui: React.ReactElement) => {
    return render(<StyledThemeProvider>{ui}</StyledThemeProvider>)
}