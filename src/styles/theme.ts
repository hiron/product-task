import { DefaultTheme } from "styled-components";

export const theme: DefaultTheme = {
  colors: {
    primary: "#fff",
    secondary: "#5C6AC4",
    buttonBorder: "#161D25",
    border: "#F1F1F1",
    font: "#161D25",
    secondaryFont: "#959EAD",
    alert: "#DE3618",
    yellow: "#EEC200",
    focus: "#E8F0D6",
  },
  grid: 12,
  layout: {
    xs: "768px",
    sm: "812px",
    md: "1080px",
  },
};
