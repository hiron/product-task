import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    grid: number;
    layout: {
      md: string;
      xs: string;
      sm: string;
    };
    colors:{
        primary: string;
        secondary: string;
        buttonBorder: string;
        border: string;
        font: string;
        secondaryFont: string;
        alert: string;
        yellow: string;
        focus: string;
    }
  }
}
