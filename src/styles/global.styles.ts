import { createGlobalStyle, DefaultTheme } from "styled-components";

export const GlobalStyles = createGlobalStyle<{ theme: DefaultTheme }>`
  @import url('https://fonts.googleapis.com/css?family=Inter:wght@300;400;500;600;700&display=swap');

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-bos;
  }

  body {
    background: #fff;
    color: #161D25;
    font-family: 'Inter', sans-serif;
    font-size: 1em;
    margin: 0;
  }

  img {
    max-width: 100%;
  }
  
`;
