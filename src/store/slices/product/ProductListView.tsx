import { useEffect } from "react";
import { Card } from "../../../components/Card";
import { Flex } from "../../../components/styles/Flex.styled";
import { Loader } from "../../../components/styles/Loader.styled";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { getProducts } from "./product-slice";

export const ProductList: React.FC = () => {
    const productsState = useAppSelector((state) => state.productsState);
    const dispatch = useAppDispatch();

    useEffect(() => { dispatch(getProducts()) }, []);
    return (<>{
        productsState.loading == "succeeded" ?
            productsState.products.map((product, i) => (<Flex key={i} md={3} sm={6} xs={12}>
                <Card {...product} />
            </Flex>))
            : <Loader />
    }
    </>);
}