import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { Product } from "../../../models/product.interface";

type ProductState = {
  loading: "idle" | "pending" | "succeeded" | "failed";
  products: Product[];
  error: string;
};

const initialState: ProductState = {
  loading: "idle",
  error: "",
  products: [],
};

export const getProducts = createAsyncThunk("products/getProducts", () => {
  return axios.get("http://localhost:3001/products").then((d) => d.data);
});

const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getProducts.pending, (state) => {
      state.loading = "pending";
    });
    builder.addCase(getProducts.fulfilled, (state, action) => {
      state.products = action.payload;
      state.loading = "succeeded";
    });
    builder.addCase(getProducts.rejected, (state, aciton: any) => {
      state.error = aciton.error.message as string;
      state.loading = "failed";
    });
  },
});

export default productSlice.reducer;
