import { useEffect } from "react";
import { CartItem } from "../../../components/CartItem";
import { Loader } from "../../../components/styles/Loader.styled";
import { useAppDispatch, useAppSelector } from "../../hooks";
//import { getCart } from "./cart-slice";

export const CartList: React.FC = () => {
    const cartState = useAppSelector((state) => state.cartState);
    const dispatch = useAppDispatch();

    //useEffect(() => { dispatch(getCart()) }, []);
    return (<>{
        cartState.loading == "succeeded" ?
            cartState.carts.map((cart, i) => (
                <CartItem key={i} {...cart} />
            ))
        : <Loader />
    }
    </>);
}