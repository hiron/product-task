import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { Cart } from "../../../models/cart.interface";
import { Product } from "../../../models/product.interface";

type CartState = {
  loading: "idle" | "pending" | "succeeded" | "failed";
  carts: Cart[];
  error: string;
  open: boolean;
};

interface UpdateProps {
  id: number;
  cartItem: Cart;
}

const initialState: CartState = {
  loading: "idle",
  error: "",
  carts: [],
  open: false,
};

export const getCart = createAsyncThunk("cart/getCart", () => {
  return axios.get("http://localhost:3001/cart").then((d) => d.data);
});

export const updateCart = createAsyncThunk(
  "cart/updateCart",
  ({ id, cartItem }: UpdateProps) => {
    return axios
      .put(`http://localhost:3001/cart/${id}`, cartItem)
      .then((d) => d.data);
  }
);

export const addItem = createAsyncThunk("cart/addItem", (body: Cart) => {
  return axios.post(`http://localhost:3001/cart/`, body).then((d) => d.data);
});

export const deleteItem = createAsyncThunk("cart/deleteItem", (id: number) => {
  return axios.delete(`http://localhost:3001/cart/${id}`).then((d) => id);
});

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    openCart: (state) => {
      state.open = true;
      state.loading = "succeeded";
    },
    closeCart: (state) => {
      state.open = false;
      state.loading = "succeeded";
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getCart.pending, (state) => {
      state.loading = "pending";
    });
    builder.addCase(getCart.fulfilled, (state, action) => {
      state.carts = action.payload;
      state.loading = "succeeded";
    });
    builder.addCase(getCart.rejected, (state, aciton: any) => {
      state.error = aciton.error.message as string;
      state.loading = "failed";
    });
    builder.addCase(updateCart.fulfilled, (state, action) => {
      let index: number = state.carts.findIndex(
        (d) => d.id == action.payload.id
      );
      state.carts[index] = action.payload;
    });

    builder.addCase(addItem.fulfilled, (state, action) => {
      state.carts.push(action.payload);
    });

    builder.addCase(deleteItem.fulfilled, (state, action) => {
      let index: number = state.carts.findIndex((d) => d.id == action.payload);
      state.carts.splice(index, 1);
    });
  },
});

export default cartSlice.reducer;

export const { openCart, closeCart } = cartSlice.actions;
