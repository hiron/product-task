import { configureStore, createReducer } from "@reduxjs/toolkit";
import ProductReducer from "./slices/product/product-slice";
import cartReducer from "./slices/cart/cart-slice";

export const store = configureStore({
  reducer: {
    productsState: ProductReducer,
    cartState: cartReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
