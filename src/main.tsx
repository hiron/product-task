import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import App from './App'
import { store } from './store/store'
import { StyledThemeProvider } from './styles/styled-theme-provider'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <StyledThemeProvider>
        <div style={{ margin: "60px 0" }}>
          <App />
        </div>
      </StyledThemeProvider>
    </Provider>

  </React.StrictMode>
)
