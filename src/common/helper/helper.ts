import { Command } from "concurrently";

export function currency(num: number): string {
  return num.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
