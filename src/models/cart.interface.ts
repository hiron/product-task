import { Product } from "./product.interface";

export interface Cart {
  id: number;
  size: string;
  quantity: number;
  total: number;
  product: Product;
}
