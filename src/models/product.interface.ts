export interface Product{
    id: number;
    title: string;
    type: string;
    rating: number;
    onSale: boolean;
    sell?: number;
    price: number;
    image: string;
}