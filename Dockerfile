FROM node:18-alpine

RUN mkdir /product

WORKDIR /product

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY package.json /product/package.json
RUN npm install

ADD . .

ENTRYPOINT ["/entrypoint.sh"]

CMD ["npm", "run", "dev"]
