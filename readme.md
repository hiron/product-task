# Installation
Clone the project first.

```sh
$ git clone https://gitlab.com/hiron/product-task.git
$ cd product-task
```
Then run the project 
```sh
$ npm run dev
```
or using docker compose

```sh
$ docker-compose up --build
```

The project will started on port 3000.
http://localhost:3000

# Screen
1st page
<img src="product.png"
     alt="Home page"
     style="float: left; margin-right: 10px;" />

& 2nd page
<img src="cart.png"
     alt="cart page"
     style="float: left; margin-right: 10px;" />

